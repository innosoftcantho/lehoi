/*
 Navicat Premium Data Transfer

 Source Server         : connection1
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : lehoi

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 15/08/2019 14:20:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `category_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_show` tinyint(1) NULL DEFAULT 0,
  `sort` tinyint(4) NOT NULL DEFAULT 0,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 1, 0, '', 'danh muc home', 0, 0, NULL, 'vi', NULL, '2019-08-10 11:11:38', '2019-08-12 09:01:16', NULL);
INSERT INTO `categories` VALUES (2, 2, 1, '', 'Danh muc 1', 1, 0, NULL, 'vi', NULL, '2019-08-10 11:12:46', '2019-08-10 11:12:59', '2019-08-10 11:12:59');
INSERT INTO `categories` VALUES (3, 3, 1, '', 'Danh muc 1', 1, 0, NULL, 'vi', NULL, '2019-08-10 11:12:46', '2019-08-10 11:12:57', '2019-08-10 11:12:57');
INSERT INTO `categories` VALUES (4, 2, 0, '', 'Danh muc 1', 1, 0, NULL, 'vi', NULL, '2019-08-10 11:13:09', '2019-08-10 14:39:16', NULL);
INSERT INTO `categories` VALUES (5, 5, 0, '', 'Danh muc 2', 1, 0, NULL, 'vi', NULL, '2019-08-10 14:39:11', '2019-08-15 09:35:55', '2019-08-15 09:35:55');
INSERT INTO `categories` VALUES (6, 6, 0, '', 'Danh muc 3', 1, 0, NULL, 'vi', NULL, '2019-08-10 14:39:26', '2019-08-10 14:39:26', NULL);
INSERT INTO `categories` VALUES (7, 7, 0, '', 'Danh muc stack', 1, 0, NULL, 'vi', NULL, '2019-08-10 14:39:59', '2019-08-10 14:39:59', NULL);
INSERT INTO `categories` VALUES (8, 8, 7, '', 'Danh muc 4', 1, 0, NULL, 'vi', NULL, '2019-08-10 14:40:07', '2019-08-10 14:40:07', NULL);
INSERT INTO `categories` VALUES (9, 9, 7, '', 'Danh muc 5', 1, 0, NULL, 'vi', NULL, '2019-08-10 14:40:16', '2019-08-10 14:40:16', NULL);
INSERT INTO `categories` VALUES (10, 10, 1, '', 'Home 1', 1, 0, NULL, 'vi', NULL, '2019-08-12 09:00:53', '2019-08-12 09:00:53', NULL);
INSERT INTO `categories` VALUES (11, 11, 0, '', 'Triễn lãm thành tựu', 1, 0, NULL, 'vi', NULL, '2019-08-13 09:19:15', '2019-08-13 09:19:15', NULL);
INSERT INTO `categories` VALUES (12, 12, 0, '', 'góc gian hàng', 1, 0, NULL, 'vi', NULL, '2019-08-13 09:19:27', '2019-08-13 09:19:27', NULL);
INSERT INTO `categories` VALUES (13, 13, 0, '', 'góc ẩm thực', 1, 0, NULL, 'vi', NULL, '2019-08-13 09:19:38', '2019-08-13 09:19:38', NULL);
INSERT INTO `categories` VALUES (14, 14, 0, '', 'tài trợ', 1, 0, NULL, 'vi', NULL, '2019-08-13 09:19:50', '2019-08-13 09:19:50', NULL);
INSERT INTO `categories` VALUES (15, 15, 0, '', 'Carousel', 1, 0, NULL, 'vi', NULL, '2019-08-14 16:37:32', '2019-08-14 16:37:32', NULL);
INSERT INTO `categories` VALUES (16, 16, 0, '', 'Banners', 1, 0, NULL, 'vi', NULL, '2019-08-14 17:18:38', '2019-08-14 17:18:38', NULL);
INSERT INTO `categories` VALUES (17, 17, 0, 'upload/categories/17.jpeg', 'Banner category', 1, 0, NULL, 'vi', NULL, '2019-08-15 09:07:26', '2019-08-15 09:07:26', NULL);

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of contacts
-- ----------------------------
INSERT INTO `contacts` VALUES (1, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:17:37', '2019-08-13 15:17:37');
INSERT INTO `contacts` VALUES (2, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:18:50', '2019-08-13 15:18:50');
INSERT INTO `contacts` VALUES (3, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:20:25', '2019-08-13 15:20:25');
INSERT INTO `contacts` VALUES (4, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:21:41', '2019-08-13 15:21:41');
INSERT INTO `contacts` VALUES (5, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:22:27', '2019-08-13 15:22:27');
INSERT INTO `contacts` VALUES (6, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:24:42', '2019-08-13 15:24:42');
INSERT INTO `contacts` VALUES (7, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:24:52', '2019-08-13 15:24:52');
INSERT INTO `contacts` VALUES (8, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:25:25', '2019-08-13 15:25:25');
INSERT INTO `contacts` VALUES (9, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:25:57', '2019-08-13 15:25:57');
INSERT INTO `contacts` VALUES (10, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:29:38', '2019-08-13 15:29:38');
INSERT INTO `contacts` VALUES (11, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:31:09', '2019-08-13 15:31:09');
INSERT INTO `contacts` VALUES (12, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:31:48', '2019-08-13 15:31:48');
INSERT INTO `contacts` VALUES (13, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:32:22', '2019-08-13 15:32:22');
INSERT INTO `contacts` VALUES (14, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:33:05', '2019-08-13 15:33:05');
INSERT INTO `contacts` VALUES (15, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:33:24', '2019-08-13 15:33:24');
INSERT INTO `contacts` VALUES (16, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:35:41', '2019-08-13 15:35:41');
INSERT INTO `contacts` VALUES (17, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:36:07', '2019-08-13 15:36:07');
INSERT INTO `contacts` VALUES (18, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:38:00', '2019-08-13 15:38:00');
INSERT INTO `contacts` VALUES (19, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:38:15', '2019-08-13 15:38:15');
INSERT INTO `contacts` VALUES (20, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:40:34', '2019-08-13 15:40:34');
INSERT INTO `contacts` VALUES (21, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:42:09', '2019-08-13 15:42:09');
INSERT INTO `contacts` VALUES (22, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:42:17', '2019-08-13 15:42:17');
INSERT INTO `contacts` VALUES (23, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:46:26', '2019-08-13 15:46:26');
INSERT INTO `contacts` VALUES (24, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:47:54', '2019-08-13 15:47:54');
INSERT INTO `contacts` VALUES (25, 'BUi hoang ngan', 'ngan@gmail.com', 'Sad', '2019-08-13 15:48:20', '2019-08-13 15:48:20');
INSERT INTO `contacts` VALUES (26, 'sad', 'Ngan@gmail.com', 'sadaf', '2019-08-13 15:56:09', '2019-08-13 15:56:09');
INSERT INTO `contacts` VALUES (27, 'ngan', 'gấ@gmail.com', 'dsafasf', '2019-08-13 16:04:19', '2019-08-13 16:04:19');
INSERT INTO `contacts` VALUES (28, 'asda', 'asf@gas.com', 'Ngan12fas', '2019-08-13 16:21:50', '2019-08-13 16:21:50');
INSERT INTO `contacts` VALUES (29, 'asda', 'asf@gas.com', 'Ngan12fas', '2019-08-13 16:22:23', '2019-08-13 16:22:23');
INSERT INTO `contacts` VALUES (30, 'asda', 'asf@gas.com', 'Ngan12fas', '2019-08-13 16:23:07', '2019-08-13 16:23:07');
INSERT INTO `contacts` VALUES (31, 'ngan', 'asnf@gmai.com', 'safafs', '2019-08-13 16:40:59', '2019-08-13 16:40:59');
INSERT INTO `contacts` VALUES (32, 'ngan', 'asnf@gmai.com', 'safafs', '2019-08-13 16:42:23', '2019-08-13 16:42:23');
INSERT INTO `contacts` VALUES (33, 'Bùi hoàng ngân', 'ngan@a.com', 'The config function gets the value of a configuration variable. The configuration values may be accessed using \"dot\" syntax, which includes the name of the file and the option you wish to access. A default value may be specified and is returned if the configuration option does not exist:', '2019-08-13 16:45:29', '2019-08-13 16:45:29');
INSERT INTO `contacts` VALUES (34, 'Bùi hoàng ngân', 'ngan@a.com', 'The config function gets the value of a configuration variable. The configuration values may be accessed using \"dot\" syntax, which includes the name of the file and the option you wish to access. A default value may be specified and is returned if the configuration option does not exist:', '2019-08-13 16:46:42', '2019-08-13 16:46:42');
INSERT INTO `contacts` VALUES (35, 'Ngan', 'ngan@gmail.com', 'Website sosad', '2019-08-14 08:53:07', '2019-08-14 08:53:07');
INSERT INTO `contacts` VALUES (36, 'Bui hoang ngan', 'nganhbui996@gmail.com', 'Bai viet lien he, test mail google.', '2019-08-14 08:54:58', '2019-08-14 08:54:58');
INSERT INTO `contacts` VALUES (37, 'sada', 'safd@sad.com', 'aDASD', '2019-08-14 09:15:40', '2019-08-14 09:15:40');
INSERT INTO `contacts` VALUES (38, 'test', 'test@k.c', 'jbkk', '2019-08-14 09:23:58', '2019-08-14 09:23:58');
INSERT INTO `contacts` VALUES (39, 'ngan', 'ngan@a.com', 'lamsl', '2019-08-14 09:25:00', '2019-08-14 09:25:00');
INSERT INTO `contacts` VALUES (40, 'test', 'test@gmail.com', 'safafs', '2019-08-14 09:26:30', '2019-08-14 09:26:30');
INSERT INTO `contacts` VALUES (41, 'sad', 'sad@gmail.com', 'safkas', '2019-08-14 09:27:30', '2019-08-14 09:27:30');
INSERT INTO `contacts` VALUES (42, 'sad', 'sa@gmail.com', 'asfasd', '2019-08-14 09:28:38', '2019-08-14 09:28:38');
INSERT INTO `contacts` VALUES (43, 'sada', 'asfas@gmail.com', 'safasg', '2019-08-14 09:29:14', '2019-08-14 09:29:14');
INSERT INTO `contacts` VALUES (44, 'safsaa', 'sadasf@gmail.com', 'Ngan1299', '2019-08-14 09:31:09', '2019-08-14 09:31:09');

-- ----------------------------
-- Table structure for content_categories
-- ----------------------------
DROP TABLE IF EXISTS `content_categories`;
CREATE TABLE `content_categories`  (
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `is_used` tinyint(1) NOT NULL DEFAULT 0,
  `is_show` tinyint(1) NOT NULL DEFAULT 0,
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `sort` bigint(20) NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of content_categories
-- ----------------------------
INSERT INTO `content_categories` VALUES (1, 4, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `content_categories` VALUES (2, 4, 0, 0, 0, 0, '2019-08-10 16:31:05', '2019-08-10 16:31:05');
INSERT INTO `content_categories` VALUES (3, 4, 0, 0, 0, 0, '2019-08-10 16:31:23', '2019-08-10 16:31:23');
INSERT INTO `content_categories` VALUES (4, 4, 0, 0, 0, 0, '2019-08-10 16:31:41', '2019-08-10 16:31:41');
INSERT INTO `content_categories` VALUES (5, 4, 0, 0, 0, 0, '2019-08-10 16:32:09', '2019-08-10 16:32:09');
INSERT INTO `content_categories` VALUES (6, 4, 0, 0, 0, 0, '2019-08-12 14:34:11', '2019-08-12 14:34:11');
INSERT INTO `content_categories` VALUES (7, 4, 0, 0, 0, 0, '2019-08-12 14:36:24', '2019-08-12 14:36:24');
INSERT INTO `content_categories` VALUES (8, 4, 0, 0, 0, 0, '2019-08-12 14:36:55', '2019-08-12 14:36:55');
INSERT INTO `content_categories` VALUES (9, 4, 0, 0, 0, 0, '2019-08-12 14:37:34', '2019-08-12 14:37:34');
INSERT INTO `content_categories` VALUES (10, 4, 0, 0, 0, 0, '2019-08-12 14:38:36', '2019-08-12 14:38:36');
INSERT INTO `content_categories` VALUES (11, 4, 0, 0, 0, 0, '2019-08-12 14:46:32', '2019-08-12 14:46:32');
INSERT INTO `content_categories` VALUES (12, 5, 0, 0, 0, 0, '2019-08-13 10:23:30', '2019-08-13 10:23:30');
INSERT INTO `content_categories` VALUES (13, 5, 0, 0, 0, 0, '2019-08-13 10:24:24', '2019-08-13 10:24:24');
INSERT INTO `content_categories` VALUES (14, 5, 0, 0, 0, 0, '2019-08-13 10:25:00', '2019-08-13 10:25:00');
INSERT INTO `content_categories` VALUES (16, 13, 0, 0, 0, 0, '2019-08-13 11:01:41', '2019-08-13 11:01:41');
INSERT INTO `content_categories` VALUES (17, 13, 0, 0, 0, 0, '2019-08-13 11:01:58', '2019-08-13 11:01:58');
INSERT INTO `content_categories` VALUES (18, 13, 0, 0, 0, 0, '2019-08-13 11:02:21', '2019-08-13 11:02:21');
INSERT INTO `content_categories` VALUES (19, 13, 0, 0, 0, 0, '2019-08-13 11:02:51', '2019-08-13 11:02:51');
INSERT INTO `content_categories` VALUES (20, 13, 0, 0, 0, 0, '2019-08-13 11:03:14', '2019-08-13 11:03:14');
INSERT INTO `content_categories` VALUES (21, 15, 0, 0, 0, 0, '2019-08-14 16:42:00', '2019-08-14 16:42:00');
INSERT INTO `content_categories` VALUES (22, 15, 0, 0, 0, 0, '2019-08-14 16:46:42', '2019-08-14 16:46:42');
INSERT INTO `content_categories` VALUES (23, 16, 0, 0, 0, 0, '2019-08-15 08:33:39', '2019-08-15 08:33:39');
INSERT INTO `content_categories` VALUES (24, 16, 0, 0, 0, 0, '2019-08-15 08:34:13', '2019-08-15 08:34:13');
INSERT INTO `content_categories` VALUES (25, 16, 0, 0, 0, 0, '2019-08-15 08:34:38', '2019-08-15 08:34:38');
INSERT INTO `content_categories` VALUES (26, 17, 0, 0, 0, 0, '2019-08-15 10:20:28', '2019-08-15 10:20:28');
INSERT INTO `content_categories` VALUES (27, 17, 0, 0, 0, 0, '2019-08-15 10:20:47', '2019-08-15 10:20:47');
INSERT INTO `content_categories` VALUES (28, 11, 0, 0, 0, 0, '2019-08-15 10:22:24', '2019-08-15 10:22:24');
INSERT INTO `content_categories` VALUES (29, 11, 0, 0, 0, 0, NULL, NULL);
INSERT INTO `content_categories` VALUES (15, 11, 0, 0, 0, 0, NULL, NULL);

-- ----------------------------
-- Table structure for contents
-- ----------------------------
DROP TABLE IF EXISTS `contents`;
CREATE TABLE `contents`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `is_show` tinyint(1) NULL DEFAULT 0,
  `is_draft` tinyint(1) NULL DEFAULT 0,
  `is_featured` tinyint(1) NULL DEFAULT 0,
  `sort` tinyint(4) NOT NULL,
  `tags` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'vi',
  `views` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `note` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of contents
-- ----------------------------
INSERT INTO `contents` VALUES (1, 1, 1, 'upload/contents/1.jpeg', 'Bai viet 1', 'bai-viet-1', 'Nani Nani Nani Nani Nani Nani Nani Nani Nani Nani Nani Nani Nani Nani Nani', '<p><span style=\"color:#c0392b\"><strong><em><u>Nani Nani Nani Nani Nani Nani Nani Nani&nbsp;Nani Nani Nani Nani Nani Nani Nani Nani&nbsp;Nani Nani Nani Nani Nani Nani Nani Nani&nbsp;Nani Nani Nani Nani Nani Nani Nani Nani&nbsp;Nani Nani Nani Nani Nani Nani Nani Nani&nbsp;Nani Nani Nani Nani Nani Nani Nani Nani&nbsp;Nani Nani Nani Nani Nani Nani Nani Nani&nbsp;Nani Nani Nani Nani Nani Nani Nani Nani&nbsp;Nani Nani Nani N</u></em></strong></span>ani Nani Nani Nani Nani&nbsp;Nani Nani Nani Nani Nani Nani Nani Nani&nbsp;Nani Nani Nani Nani Nani Nani Nani Nani&nbsp;Nani Nani Nani Nani Nani Nani Nani Nani&nbsp;Nani Nani Nani Nani Nani Nani Nani Nani&nbsp;Nani Nani Nani Nani Nani Nani Nani Nani&nbsp;Nani Nani Nani Nani Nani Nani Nani Nani&nbsp;</p>', 1, 0, 1, 0, '', NULL, 'vi', 0, NULL, '2019-08-10 15:22:00', '2019-08-12 10:56:28', NULL);
INSERT INTO `contents` VALUES (2, 2, 1, 'upload/contents/2.jpeg', 'bai viet 2', 'bai-viet-2', 'asdasdasf', '<p>safasfasfasfasfa</p>', 1, 0, 1, 0, '', NULL, 'vi', 0, NULL, '2019-08-10 16:31:05', '2019-08-12 10:56:20', NULL);
INSERT INTO `contents` VALUES (3, 3, 1, 'upload/contents/3.jpeg', 'Bai viet 3', 'bai-viet-3', 'safafasfasfaaf', '<p>safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;safafsaf&nbsp;</p>', 1, 0, 1, 0, '', NULL, 'vi', 0, NULL, '2019-08-10 16:31:23', '2019-08-12 10:56:13', NULL);
INSERT INTO `contents` VALUES (4, 4, 1, 'upload/contents/4.jpeg', 'Bai viet 4', 'bai-viet-4', 'sada asdas da asda sada asdas da asda sada asdas da asda sada asdas da asda sada asdas da asda sada asdas da asda sada asdas da asda', '<p>sada asdas da asda&nbsp;sada asdas da asda&nbsp;sada asdas da asda</p>', 1, 0, 1, 0, '', NULL, 'vi', 0, NULL, '2019-08-10 16:31:41', '2019-08-12 10:14:13', NULL);
INSERT INTO `contents` VALUES (5, 5, 1, 'upload/contents/5.jpeg', 'bai viet 5', 'bai-viet-5', 'sada asd a asdas sada sadas  asdas  asda sda as dad sada asd a asdas sada sadas  asdas  asda sda as dad sada asd a asdas sada sadas  asdas  asda sda as dad', '<p>&nbsp;asdasda as dsaa s asd as aa</p>', 1, 0, 1, 0, '', NULL, 'vi', 0, NULL, '2019-08-10 16:32:09', '2019-08-12 10:14:06', NULL);
INSERT INTO `contents` VALUES (6, 6, 1, 'upload/contents/6.jpeg', 'Bai viet 6', 'bai-viet-6', 'Remember, Eloquent will automatically determine the proper foreign key column on the  Comment model.', '<p>A one-to-many relationship is used to define relationships where a single model owns any amount of other models. For example, a blog post may have an infinite number of comments. Like all other Eloquent relationships, one-to-many relationships are defined by placing a function on your Eloquent model:</p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-12 14:34:11', '2019-08-12 14:34:24', NULL);
INSERT INTO `contents` VALUES (7, 7, 1, 'upload/contents/7.jpeg', 'Bai viet 7', 'bai-viet-7', 'Remember, Eloquent will automatically determine the proper foreign key column on the  Comment model. By convention,', '<p>Eloquent will take the &quot;snake case&quot; name of the owning model and suffix it with _id. So, for this example, <s>Eloquent will assume the foreign key on the &nbsp;Comment model is post_id.</s></p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-12 14:36:24', '2019-08-12 14:37:08', NULL);
INSERT INTO `contents` VALUES (8, 8, 1, 'upload/contents/8.jpeg', 'bai viet 8', 'bai-viet-8', 'Remember, Eloquent will automatically determine the proper foreign key column', '<p>Remember, Eloquent will automatically determine the proper foreign key column on the&nbsp;<code>Comment</code>&nbsp;model. By convention, Eloquent will take the &quot;snake case&quot; name of the owning model and suffix it with&nbsp;<code>_id</code>. So, for this example, Eloquent will assume the foreign key on the&nbsp;<code>Comment</code>&nbsp;model is&nbsp;<code>post_id</code>.</p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-12 14:36:55', '2019-08-12 14:36:55', NULL);
INSERT INTO `contents` VALUES (9, 9, 1, 'upload/contents/9.jpeg', 'Bai viet 8', 'bai-viet-8', 'Remember, Eloquent will automatically determine the proper foreign key column on the Comment model. By convention', '<p>Remember, Eloquent will automatically determine the proper foreign key column on the&nbsp;<code>Comment</code>&nbsp;model. By convention, Eloquent will take the &quot;snake case&quot; name of the owning model and suffix it with&nbsp;<code>_id</code>. So, for this example, Eloquent will assume the foreign key on the&nbsp;<code>Comment</code>&nbsp;model is&nbsp;<code>post_id</code>.</p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-12 14:37:34', '2019-08-12 14:37:34', NULL);
INSERT INTO `contents` VALUES (10, 10, 1, 'upload/contents/10.jpeg', 'Bai viet 9', 'bai-viet-9', 'Remember, Eloquent will automatically determine the proper foreign key column on the Comme', '<p>Remember, Eloquent will automatically determine the proper foreign key column on the&nbsp;<code>Comment</code>&nbsp;model. By convention, Eloquent will take the &quot;snake case&quot; name of the owning model and suffix it with&nbsp;<code>_id</code>. So, for this example, Eloquent will assume the foreign key on the&nbsp;<code>Comment</code>&nbsp;model is&nbsp;<code>post_id</code>.</p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-12 14:38:36', '2019-08-12 14:39:28', NULL);
INSERT INTO `contents` VALUES (11, 11, 1, 'upload/contents/11.jpeg', 'Bai viet 10', 'bai-viet-10', 'The links method will render the links to the rest of the pages in the result set. Each of these links wil', '<p>The&nbsp;<code>links</code>&nbsp;method will render the links to the rest of the pages in the result set. Each of these links will already contain the proper&nbsp;<code>page</code>&nbsp;query string variable. Remember, the HTML generated by the&nbsp;<code>links</code>&nbsp;method is compatible with the&nbsp;<a href=\"https://getbootstrap.com/\">Bootstrap CSS framework</a>.</p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-12 14:46:32', '2019-08-12 14:46:32', NULL);
INSERT INTO `contents` VALUES (12, 12, 1, 'upload/contents/12.jpeg', 'danh muc 2 bai viet 1', 'danh-muc-2-bai-viet-1', 'orem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt.', '<p>You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.&nbsp;You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.</p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-13 10:23:30', '2019-08-13 10:23:55', NULL);
INSERT INTO `contents` VALUES (13, 13, 1, 'upload/contents/13.jpeg', 'danh muc 2 bai viet 2', 'danh-muc-2-bai-viet-2', 'You can modify the variables to your own custom values, or just use the mixins with their default values.', '<p>You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.</p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-13 10:24:24', '2019-08-13 10:24:24', NULL);
INSERT INTO `contents` VALUES (14, 14, 1, 'upload/contents/14.jpeg', 'Danh muc 2 bai viet 3', 'danh-muc-2-bai-viet-3', 'You can modify the variables to your own custom values, or just use the mixins with their default values', '<p>You can mod<em><u>ify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.You can modify the variables to your own custom values, or just use the mixins with t</u></em>heir default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.</p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-13 10:25:00', '2019-08-13 10:25:00', NULL);
INSERT INTO `contents` VALUES (15, 15, 1, 'upload/contents/15.jpeg', 'Danh muc 2 bai viet 4', 'danh-muc-2-bai-viet-4', 'You can modify the variables to your own custom values, or just use the mixins with their default val', '<p>You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.</p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-13 10:25:23', '2019-08-13 10:25:23', NULL);
INSERT INTO `contents` VALUES (16, 16, 1, 'upload/contents/16.jpeg', 'Am thuc 1', 'am-thuc-1', 'You can modify the variables to your own custom values, or just use the mixins with their default values.', '<p>You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.</p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-13 11:01:41', '2019-08-13 11:01:41', NULL);
INSERT INTO `contents` VALUES (17, 17, 1, 'upload/contents/17.gif', 'Am thuc 2', 'am-thuc-2', 'You can modify the variables to your own custom values, or just use the mixins with their default values', '<p>You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.</p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-13 11:01:58', '2019-08-13 11:01:58', NULL);
INSERT INTO `contents` VALUES (18, 18, 1, 'upload/contents/18.jpeg', 'Am thuc 3', 'am-thuc-3', 'You can modify the variables to your own custom values, or just use the mixins with their default values.', '<p>You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.</p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-13 11:02:21', '2019-08-13 11:02:35', NULL);
INSERT INTO `contents` VALUES (19, 19, 1, 'upload/contents/19.jpeg', 'Am thuc 4', 'am-thuc-4', 'You can modify the variables to your own custom values, or just use the mixins with their default values.', '<p>You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.</p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-13 11:02:51', '2019-08-13 11:02:58', NULL);
INSERT INTO `contents` VALUES (20, 20, 1, 'upload/contents/20.gif', 'Am thuc 5 Am thuc 5 Am thuc 5 Am thuc 5 Am thuc 5', 'am-thuc-5-am-thuc-5-am-thuc-5-am-thuc-5-am-thuc-5', 'You can modify the variables to your own custom values, or just use the mixins with their default values', '<p>You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.You can modify the variables to your own custom values, or just use the mixins with their default values. Here&rsquo;s an example of using the default settings to create a two-column layout with a gap between.</p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-13 11:03:14', '2019-08-13 14:17:40', NULL);
INSERT INTO `contents` VALUES (21, 21, 1, 'upload/contents/21.jpeg', 'carousel 1', 'carousel-1', NULL, NULL, 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-14 16:42:00', '2019-08-14 16:42:09', NULL);
INSERT INTO `contents` VALUES (22, 22, 1, 'upload/contents/22.jpeg', 'Carousel 2', 'carousel-2', NULL, NULL, 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-14 16:46:41', '2019-08-14 16:46:41', NULL);
INSERT INTO `contents` VALUES (23, 23, 1, 'upload/contents/23.jpeg', 'Banner 1', 'banner-1', NULL, NULL, 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-15 08:33:39', '2019-08-15 08:33:39', NULL);
INSERT INTO `contents` VALUES (24, 24, 1, 'upload/contents/24.jpeg', 'Banner 2', 'banner-2', NULL, NULL, 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-15 08:34:13', '2019-08-15 08:34:13', NULL);
INSERT INTO `contents` VALUES (25, 25, 1, 'upload/contents/25.jpeg', 'Banner 3', 'banner-3', NULL, NULL, 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-15 08:34:38', '2019-08-15 08:34:38', NULL);
INSERT INTO `contents` VALUES (26, 26, 1, 'upload/contents/26.jpeg', 'Banner category', 'banner-category', NULL, NULL, 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-15 10:20:28', '2019-08-15 10:20:28', NULL);
INSERT INTO `contents` VALUES (27, 27, 1, 'upload/contents/27.jpeg', 'Banner category 2', 'banner-category-2', NULL, NULL, 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-15 10:20:47', '2019-08-15 10:20:47', NULL);
INSERT INTO `contents` VALUES (28, 28, 1, 'upload/contents/28.jpeg', 'Trien lam 1', 'trien-lam-1', 'The @error directive may be used to quickly check if validation error messages exist for a given attribute. Wit', '<p>The&nbsp;<code>@error</code>&nbsp;directive may be used to quickly check if&nbsp;<a href=\"https://laravel.com/docs/5.8/validation#quick-displaying-the-validation-errors\">validation error messages</a>&nbsp;exist for a given attribute. Within an&nbsp;<code>@error</code>&nbsp;directive, you may echo the&nbsp;<code>$message</code>&nbsp;variable to display the error message:The&nbsp;<code>@error</code>&nbsp;directive may be used to quickly check if&nbsp;<a href=\"https://laravel.com/docs/5.8/validation#quick-displaying-the-validation-errors\">validation error messages</a>&nbsp;exist for a given attribute. Within an&nbsp;<code>@error</code>&nbsp;directive, you may echo the&nbsp;<code>$message</code>&nbsp;variable to display the error message:The&nbsp;<code>@error</code>&nbsp;directive may be used to quickly check if&nbsp;<a href=\"https://laravel.com/docs/5.8/validation#quick-displaying-the-validation-errors\">validation error messages</a>&nbsp;exist for a given attribute. Within an&nbsp;<code>@error</code>&nbsp;directive, you may echo the&nbsp;<code>$message</code>&nbsp;variable to display the error message:</p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-15 10:22:23', '2019-08-15 10:22:24', NULL);
INSERT INTO `contents` VALUES (29, 29, 1, 'upload/contents/29.jpeg', 'Trien lam thanh tuu 2', 'trien-lam-thanh-tuu-2', 'The @error directive may be used to quickly check if validation error messages', '<p>The&nbsp;<code>@error</code>&nbsp;directive may be used to quickly check if&nbsp;<a href=\"https://laravel.com/docs/5.8/validation#quick-displaying-the-validation-errors\">validation error messages</a>&nbsp;exist for a given attribute. Within an&nbsp;<code>@error</code>&nbsp;directive, you may echo the&nbsp;<code>$message</code>&nbsp;variable to display the error message:The&nbsp;<code>@error</code>&nbsp;directive may be used to quickly check if&nbsp;<a href=\"https://laravel.com/docs/5.8/validation#quick-displaying-the-validation-errors\">validation error messages</a>&nbsp;exist for a given attribute. Within an&nbsp;<code>@error</code>&nbsp;directive, you may echo the&nbsp;<code>$message</code>&nbsp;variable to display the error message:The&nbsp;<code>@error</code>&nbsp;directive may be used to quickly check if&nbsp;<a href=\"https://laravel.com/docs/5.8/validation#quick-displaying-the-validation-errors\">validation error messages</a>&nbsp;exist for a given attribute. Within an&nbsp;<code>@error</code>&nbsp;directive, you may echo the&nbsp;<code>$message</code>&nbsp;variable to display the error message:</p>', 1, 0, 0, 0, '', NULL, 'vi', 0, NULL, '2019-08-15 10:23:18', '2019-08-15 10:23:32', NULL);

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lang` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NULL DEFAULT 0,
  `note` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menu_categories
-- ----------------------------
DROP TABLE IF EXISTS `menu_categories`;
CREATE TABLE `menu_categories`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `category_id`) USING BTREE,
  INDEX `menu_categories_category_id_foreign`(`category_id`) USING BTREE,
  CONSTRAINT `menu_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_categories_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_categories
-- ----------------------------
INSERT INTO `menu_categories` VALUES (1, 4, NULL, NULL, NULL);
INSERT INTO `menu_categories` VALUES (1, 11, NULL, NULL, NULL);
INSERT INTO `menu_categories` VALUES (1, 13, NULL, NULL, NULL);
INSERT INTO `menu_categories` VALUES (1, 15, NULL, NULL, NULL);
INSERT INTO `menu_categories` VALUES (1, 16, NULL, NULL, NULL);
INSERT INTO `menu_categories` VALUES (2, 4, '2019-08-10 14:44:06', '2019-08-10 14:44:06', NULL);
INSERT INTO `menu_categories` VALUES (3, 7, '2019-08-13 10:49:46', '2019-08-13 10:49:46', NULL);
INSERT INTO `menu_categories` VALUES (4, 8, '2019-08-10 14:45:34', '2019-08-10 14:45:34', NULL);
INSERT INTO `menu_categories` VALUES (6, 11, '2019-08-13 09:25:21', '2019-08-13 09:25:21', NULL);
INSERT INTO `menu_categories` VALUES (7, 12, '2019-08-13 09:25:51', '2019-08-13 09:25:51', NULL);
INSERT INTO `menu_categories` VALUES (8, 13, '2019-08-13 09:26:12', '2019-08-13 09:26:12', NULL);
INSERT INTO `menu_categories` VALUES (9, 14, '2019-08-13 09:26:28', '2019-08-13 09:26:28', NULL);
INSERT INTO `menu_categories` VALUES (12, 6, NULL, NULL, NULL);
INSERT INTO `menu_categories` VALUES (12, 17, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for menu_contents
-- ----------------------------
DROP TABLE IF EXISTS `menu_contents`;
CREATE TABLE `menu_contents`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `content_id`) USING BTREE,
  INDEX `menu_contents_content_id_foreign`(`content_id`) USING BTREE,
  CONSTRAINT `menu_contents_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_contents_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `menu_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '\"\"',
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `sort` tinyint(4) NOT NULL,
  `lang` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES (1, 1, 0, 'Trang chu', '/', 'home', '', 1, 0, 'vi', NULL, '2019-08-10 14:38:25', '2019-08-10 14:58:04');
INSERT INTO `menus` VALUES (2, 2, 0, 'Danh muc 1', 'danh-muc-1', 'category', NULL, 1, 0, 'vi', NULL, '2019-08-10 14:44:06', '2019-08-10 14:44:06');
INSERT INTO `menus` VALUES (3, 3, 0, 'stack', 'stack', 'category', '', 1, 0, 'vi', NULL, '2019-08-10 14:44:45', '2019-08-13 10:49:46');
INSERT INTO `menus` VALUES (4, 4, 3, 'Danh muc 4', 'danh-muc-4', 'category', NULL, 1, 0, 'vi', NULL, '2019-08-10 14:45:34', '2019-08-10 14:45:34');
INSERT INTO `menus` VALUES (5, 5, 0, 'Lien he', 'lien-he', 'contact', NULL, 1, 0, 'vi', NULL, '2019-08-10 14:46:32', '2019-08-10 14:46:32');
INSERT INTO `menus` VALUES (6, 6, 0, 'triễn lãm thành tựu', 'trien-lam-thanh-tuu', 'category', NULL, 1, 0, 'vi', NULL, '2019-08-13 09:25:21', '2019-08-13 09:25:21');
INSERT INTO `menus` VALUES (7, 7, 0, 'góc gian hàng', 'goc-gian-hang', 'category', NULL, 1, 0, 'vi', NULL, '2019-08-13 09:25:51', '2019-08-13 09:25:51');
INSERT INTO `menus` VALUES (8, 8, 0, 'góc ẩm thực', 'goc-am-thuc', 'category', NULL, 1, 0, 'vi', NULL, '2019-08-13 09:26:12', '2019-08-13 09:26:12');
INSERT INTO `menus` VALUES (9, 9, 0, 'tài trợ', 'tai-tro', 'category', NULL, 1, 0, 'vi', NULL, '2019-08-13 09:26:28', '2019-08-13 09:26:28');
INSERT INTO `menus` VALUES (12, 10, 0, 'Danh muc 3', 'danh-muc-3', 'category', '', 1, 0, 'vi', NULL, '2019-08-15 09:46:14', '2019-08-15 09:48:44');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_05_145452_create_categories_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_08_05_145513_create_contents_table', 1);
INSERT INTO `migrations` VALUES (5, '2019_08_05_145727_create_languages_table', 1);
INSERT INTO `migrations` VALUES (6, '2019_08_05_145742_create_menus_table', 1);
INSERT INTO `migrations` VALUES (7, '2019_08_05_145908_create_menu_categories_table', 1);
INSERT INTO `migrations` VALUES (8, '2019_08_05_145917_create_menu_contents_table', 1);
INSERT INTO `migrations` VALUES (9, '2019_08_06_092818_create_content_categories_table', 1);
INSERT INTO `migrations` VALUES (10, '2019_08_13_143913_create_contacts_table', 1);
INSERT INTO `migrations` VALUES (11, '2019_08_14_104127_create_sponsors_table', 1);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sponsors
-- ----------------------------
DROP TABLE IF EXISTS `sponsors`;
CREATE TABLE `sponsors`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `sponsor_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `note` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT 'text',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sponsors
-- ----------------------------
INSERT INTO `sponsors` VALUES (4, 'upload/contents/4.jpeg', 'nha tai tro 1', NULL, '2019-08-14 11:23:56', '2019-08-14 11:25:59');
INSERT INTO `sponsors` VALUES (5, 'upload/contents/5.png', 'nha tai tro 2', NULL, '2019-08-14 11:27:40', '2019-08-14 11:27:40');
INSERT INTO `sponsors` VALUES (6, 'upload/contents/6.png', 'nha tai tro 3', NULL, '2019-08-14 11:27:52', '2019-08-14 11:27:52');
INSERT INTO `sponsors` VALUES (7, 'upload/contents/7.jpeg', 'Asus', NULL, '2019-08-14 14:20:03', '2019-08-14 14:20:03');
INSERT INTO `sponsors` VALUES (8, 'upload/contents/8.png', 'Steam', NULL, '2019-08-14 14:20:15', '2019-08-14 14:20:15');
INSERT INTO `sponsors` VALUES (9, 'upload/contents/9.jpeg', 'Microsoft', NULL, '2019-08-14 14:35:36', '2019-08-14 14:35:36');
INSERT INTO `sponsors` VALUES (11, 'upload/sponsors/11.png', 'Green', NULL, '2019-08-15 08:46:58', '2019-08-15 08:52:30');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_username_unique`(`username`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', 'admin', 'nganhbui996@gmail.com', NULL, '$2y$10$dDS9uo43z/lB4ceaCkxM1.gpUhGAxmjdROQY30g4ZPdpH0UZPPE5i', 1, 'SyHD9xNntRUldlxIwPUuCoy0vTQmnhaBIgc82NBbHkQXBxZwccoF4kQP3X4O', NULL, '2019-08-15 08:29:00', NULL);

SET FOREIGN_KEY_CHECKS = 1;
