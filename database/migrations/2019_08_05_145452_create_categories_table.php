<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('parent_id');
            $table->string('avatar', 191)->nullable()->default('');
            $table->string('category_name', 191)->nullable(false);
            $table->boolean('is_show')->nullable()->default(0);
            $table->tinyInteger('sort')->default(0);
            $table->longText('description')->nullable();
            $table->string('lang', 20);
            $table->string('note', 191)->nullable()->default('');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
