<?php 

return [
    'lists'                 => 'Danh sách sản phẩm',
    'detail'                => 'Chi tiết sản phẩm',
    'description'           => 'Mô tả sản phẩm',

    'alias'                 => 'Alias',
    'avatar'                => 'Ảnh',
    'bill_price'            => 'Giá bán',
    'is_show'               => 'Hiển thị',
    'no_show'               => 'Không hiển thị',
    'note'                  => 'Ghi chú',
    'origin'                => 'Xuất xứ',
    'product_code'          => 'Mã sản phẩm',
    'product_name'          => 'Tên sản phẩm',
    'receipt_price'         => 'Giá nhập',
    'show'                  => 'Hiển thị',
    'stock'                 => 'Tồn kho',
    'type_id'               => 'Loại',
    'unit'                  => 'Đơn vị tính',
    'upload'                => 'Ảnh tải lên',
    'video'                 => 'Video',
    'warranty'              => 'Bảo hành'
];