<?php 

return [
    'detail'        => 'Chi tiết',
    'lists'         => 'Danh sách ngôn ngữ',

    'avatar'        => 'Ảnh',
    'sponsor_name'  => 'Tên nhà tài trợ',
    'is_show'       => 'Hiển thị',
    'is_hide'       => 'Ẩn',
    'sort'          => 'Sắp xếp',
    'target'        => 'Cách chuyển trang',
    'normal'        => 'Bình thường',
    'blank'         => 'Cửa sổ mới',

    'url'           => 'Url',
    'note'          => 'Ghi chú',
];