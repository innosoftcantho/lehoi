<?php 

return [
    'list'                  => 'Danh sách mức giá',
    'detail'                => 'Chi tiết mức giá',
    'description'           => 'Mô tả mức giá',

    'price_name'            => 'Tên mức giá',
    'price'                 => 'Giá',
    'sort'                  => 'Sắp xếp',
    'note'                  => 'Ghi chú',
];