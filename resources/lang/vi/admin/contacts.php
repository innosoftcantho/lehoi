<?php 

return [
    'detail'            => 'Chi tiết',
    'is_default'        => 'Ngôn ngữ mặc định',
    'customer_name'     => 'Tên khách hàng',
    'email'             => 'Email',
    'content'           => 'Nội dung',
    'lists'             => 'Danh sách ngôn ngữ',
    'note'              => 'Ghi chú',
];