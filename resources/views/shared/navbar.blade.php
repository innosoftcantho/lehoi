<nav class="navbar navbar-expand-sm navbar-top d-none d-xl-block py-2">
    <div class="container">
        <ul class="navbar-nav w-100">
            <li class="nav-item">
                <a class="text-secondary" href="//facebook.com"><i class="fab fa-facebook-f"></i></a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
                <a class="text-secondary mx-2" href="//instagram.com"><i class="fab fa-instagram"></i></a>
            </li>
            <li class="nav-item">
                <a class="text-secondary" href="//youtube.com"><i class="fab fa-youtube"></i></a>
            </li>
            <li class="nav-item ml-auto">
                <a href="{{ route('admin') }}" class="text-secondary login pr-2">Đăng nhập</a>
            </li>
        </ul>
    </div>
</nav>
<nav class="navbar navbar-expand-sm header2 navbar-mid bg-white d-none d-xl-block py-lg-0">
    <div class="container">
        <a class="py-0" href="{{ url("/") }}" id="logoNavScroll">
            <img src="{{ asset('img/logo.png') }}" class="navbar-brand">
        </a>
        <div class="text-center">
            <h4 class="text-dark text-uppercase font-weight-bold mt-2 mb-0 pt-1">
                chương trình
            </h4>
            <h2 class="text-uppercase text-warning font-weight-black mt-2 mb-2 pb-1">
                lễ hội cá tra việt nam
            </h2>
        </div>
        <a class="py-0" href="{{ url("/") }}" id="logoNavScroll">
            <img src="{{ asset('img/logo2.png') }} " class="navbar-brand rounded-circle mr-0">
        </a>
    </div>
</nav>
<nav class="navbar navbar-expand-xl navbar-dark bg-dark header2 navbar-bot shadow py-lg-0 px-0 px-lg-3">
    <div class="container">
        <button class="navbar-toggler border-0 my-0 my-lg-2 px-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="text-center d-flex d-xl-none mx-auto">
            <h3 class="text-uppercase text-warning font-weight-black mb-0">
                lễ hội cá tra việt nam
            </h3>
        </div>
        <div class="logo-bot d-flex d-xl-none">
            <a class="navbar-brand py-0 mr-2" href="#" id="logoNavScroll">
                <img src="{{ asset('img/logo.png') }}">
            </a>
            <a class="navbar-brand py-0 mr-0" href="#" id="logoNavScroll">
                <img src="{{ asset('img/logo2.png') }} " class="rounded-circle">
            </a>
        </div>
        <div class="collapse navbar-collapse text-left my-2 my-lg-0" id="navbarSupportedContent">
            <ul class="navbar-nav w-100">
                @foreach ($menus as $loopMenu)
                    @if (count($loopMenu->children) == 0)
                    <li class="nav-item mx-xl-4 pt-xl-3 {{ $menu->id == $loopMenu->id ? "active" : "" }}">
                        <a class="nav-link text-uppercase font-weight-bold px-0 py-xl-0" href="{{ url($loopMenu->alias) }}">{{ $loopMenu->menu_name }}<span class="sr-only">(current)</span></a>
                    </li>
                    @else
                    <li class="nav-item dropdown mx-xl-4 pt-xl-3 {{ $menu->parent ? $menu->parent->id : "" == $loopMenu->id ? "active" : "" }}">
                        <a class="nav-link dropdown-toggle font-weight-bold text-uppercase px-0 py-xl-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" 
                        aria-haspopup="true" aria-expanded="false">
                            {{ $loopMenu->menu_name }}
                        </a>
                        <div class="dropdown-menu text-left text-xl-center bg-dark border-0" aria-labelledby="navbarDropdown">
                            @foreach ($loopMenu->children as $children)
                            <a class="dropdown-item font-weight-bold text-uppercase {{ $menu->id == $children->id ? "yellow" : "" }}" href="{{ url($children->alias) }}">{{ $children->menu_name }}</a>
                            @endforeach
                        </div>
                    </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
</nav>
