<nav class="navbar navbar-expand-lg navbar-dark p-0 shadow">
    <div class="collapse navbar-collapse align-items-start bg-danger sidebar" id="sidebar">
        <ul class="flex-column nav nav-pills nav-fill w-100">
            <a class="nav-header">ĐIỀU HƯỚNG</a>
            <a class="nav-link {{ $route=='categories' ? 'active' : '' }}" href="{{ route('categories.index') }}">
                <i class="fas fa-bookmark"></i>
                <span class="pl-3">Danh mục</span>
            </a>
            <a class="nav-link {{ $route=='contents' ? 'active' : '' }}" href="{{ route('contents.index') }}">
                <i class="fas fa-book"></i>
                <span class="pl-3">Bài viết</span>
            </a>
            <a class="nav-link {{ $route=='menus' ? 'active' : '' }}" href="{{ route('menus.index') }}">
                <i class="fas fa-clipboard-list"></i>
                <span class="pl-3">Menu</span>
            </a>
            <a class="nav-link {{ $route=='languages' ? 'active' : '' }}" href="{{ route('languages.index') }}">
                <i class="fas fa-language"></i>
                <span class="pl-3">Ngôn ngữ</span>
            </a>
            <a class="nav-header">CƠ BẢN</a>
            <a class="nav-link {{ $route=='contacts' ? 'active' : '' }}" href="{{ route('contacts.index') }}">
                <i class="far fa-envelope"></i>
                <span class="pl-3">Liên hệ</span>
            </a>
            <a class="nav-link {{ $route=='sponsors' ? 'active' : '' }}" href="{{ route('sponsors.index') }}">
                <i class="fas fa-briefcase"></i>
                <span class="pl-3">Nhà tài trợ</span>
            </a>
            @if (Auth::id() < 2)
            <a class="nav-header">NHÂN SỰ</a>
            <a class="nav-link {{ $route=='users' ? 'active' : '' }}" href="{{ route('users.index') }}">
                <i class="fas fa-users"></i>
                <span class="pl-3">Người dùng</span>
            </a>
            @endif

            <div class="d-lg-none">
                <div class="dropdown">
                    <a id="navbarProfile" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->fullname }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarProfile">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                    </div>
                </div>
            </div>
        </ul>
    </div>
</nav>