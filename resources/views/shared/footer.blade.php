@section('footer')

<div class="footer text-white bg-center bg-cover" style="background-image: url( {{ asset('img/giai1.jpg') }} )">
    <div class="bg-overlay py-4">
        <div class="container">
            <div class="row py-5">
                <h3 class="col-lg-4 text-center font-weight-normal mb-4 mb-lg-0">
                    <div class="image d-flex align-items-center bg-white rounded-circle mx-auto">
                        <i class="fas fa-map-marker-alt text-dark mx-auto"></i>
                    </div>
                    <div class="mt-3">
                        Tầng 4, Toà nhà VCCI, Số 12 Hoà Bình, Phường An Cư, Quận Ninh Kiều, Thành phố Cần Thơ
                    </div>
                </h3>
                <h3 class="col-lg-4 text-center font-weight-normal mb-4 mb-lg-0">
                    <div class="image d-flex align-items-center bg-white rounded-circle mx-auto">
                        <i class="fas fa-phone-alt text-dark mx-auto"></i>
                    </div>
                    <div class="mt-3">
                        <div>
                            Điện thoại: 0292 3819091
                        </div>
                        <div>
                            Fax: 0292 3819003
                        </div>
                    </div>
                </h3>
                <h3 class="col-lg-4 text-center font-weight-normal mb-0 mb-lg-4 mb-lg-0">
                    <div class="image d-flex align-items-center bg-white rounded-circle mx-auto">
                        <i class="fas fa-envelope text-dark mx-auto"></i>
                    </div>
                    <div class="mt-3">
                        vietnam.office@vnpangasius.com
                    </div>
                </h3>
            </div>
        </div>
    </div>
    <h5 class="copyright bg-dark py-4 mb-0" align="center">
        Copyright © 2019 Hiệp Hội Cá Tra Việt Nam
    </h5>
</div>