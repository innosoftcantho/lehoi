@extends('layouts.web')
@section('content')
    <div class="container">
        <form action="{{ route('web.contacts.store') }}" method="post">
            @csrf
            <div class="col-md-7 mx-auto my-5 px-0 px-sm-3">
                <h2 class="text-uppercase text-center font-weight-bold mb-4">gửi liên hệ cho chúng tôi</h2>
                <div class="row">
                    <div class="col-12 col-lg-6 mb-3">
                    <input class="form-control" type="text" name="customer_name" placeholder="Họ và tên" >
                    </div>
                    <div class="col-12 col-lg-6 mb-3">
                        <input class="form-control" type="text" name="email" placeholder="Email" >
                    </div>
                </div>
                <textarea class="form-control my-md-2" name="content" id="exampleFormControlTextarea1" placeholder="Nội dung liên hệ" rows="8" style="resize: none;" ></textarea>
                <div class="mt-4">
                    <button type="submit" class="btn btn-dark text-uppercase btn-block"><h3 class="mt-2">gửi</h3></button>
                </div>
            </div>
        </form>
    </div>
    <div class="bg-center bg-cover map w-100">
        <iframe id="gmap_canvas" width="100%" height="100%" src="https://maps.google.com/maps?q=can%20tho%20%20Hi%E1%BB%87p%20h%E1%BB%99i%20C%C3%A1%20tra%20VN%20&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
    </div>
@endsection
@push('ready')
    @if ($message = Session::get('info'))
    toastr["info"]("{{$message }}");
    @endif
    @if (count($errors) > 0)
    toastr["error"]("@foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach");
    @endif
@endpush