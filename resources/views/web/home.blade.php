@extends('layouts.web')
@section('content')

<!-- Header Slideshow -->
@include('web.home.slideshow')

<!-- Section News -->
@include('web.home.news')

<!-- Section Overview -->
@include('web.home.news_2')

<!-- Section News 2 -->
@include('web.home.news_3')

<!-- Body Slideshow -->
@include('web.home.sponsors')

@endsection

@push('js')
    <script>
        if (screen.width >= 992) {
            new WOW().init();
        }
    </script>
@endpush
