@extends('layouts.web')
@section('content')
    <div class="content">
        <div class="container py-5">
            <div class="row">
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="font-weight-bold">{{ $content->title }}</h2>
                            <h4 class="text-secondary my-3 py-2">{{ $content->user->username }} - {{ date('d-m-Y H:i', strtotime($content->created_at)) }}</h4>
                        </div>
                        <div class="col-12">
                            <div class="text-justify text-body">
                                {!! $content->content !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="bg-light border px-4 py-3 mt-4 mt-lg-0">
                        <h3 class="text-uppercase font-weight-bold mb-4">danh mục bài viết</h3>
                        <ul class="navbar-nav flex-column d-block text-uppercase sidebar">
                            @foreach ($categories as $category)
                            @if (count($category->children) > 0)
                            <li class="text-uppercase {{ (!$loop->first && !$loop->last) ? "my-1" : "" }}">
                                <a href="#category_{{ $category->id }}" data-toggle="collapse" aria-expanded="false">
                                    <span>{{ $category->menu_name }}</span>
                                    <span class="dropdown-toggle dropdown-toggle-split">
                                    </span>
                                </a>
                                <ul class="collapse list-unstyled pl-4" id="category_{{ $category->id }}">
                                    @foreach ($category->children as $children)
                                    <li>
                                        <a href="{{ url($children->alias) }}">{{ $children->menu_name }}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @else
                            <li class="text-uppercase {{ (!$loop->first && !$loop->last) ? "my-3" : "" }}">
                                <a href="{{ url($category->alias) }}">
                                    <span>{{ $category->menu_name }}</span>
                                </a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>
                    <h3 class="text-uppercase font-weight-bold border-dark mt-4">tương tự</h3>
                    <div class="row">
                        @foreach ($contents as $content)
                        <div class="col-12 mb-2">
                            <a href="{{ url($menu->alias, [$content->alias]) }}">
                                <div class="card border-0 mt-3 w-100">
                                    <img src="{{ asset($content->avatar)}}" class="w-100">
                                    <div class="card-body p-0 pt-2">
                                        <h4 class="card-title font-weight-bold mb-0">
                                            {{ $content->title }}
                                        </h4>
                                        <small class="card-text font-italic my-1">{{ $content->user->username }} - {{ \Carbon\Carbon::parse($content->created_at)->diffForHumans() }}</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection