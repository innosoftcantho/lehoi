<div class="sponsors bg-light pb-5">
    <div class="container pt-3">
        <h2 class="text-uppercase font-weight-bold wow fadeInRight my-3 pb-3">
            đơn vị phối hợp
        </h2>
        @if(count($sponsors) > 0)
        <div class="carousel slide modal-flick wow fadeInRight"data-type="multi" data-interval="3000" data-flickity='{
            "cellAlign": "center",
            "pageDots": false,
            "wrapAround": {{ (count($sponsors) > 5) ? "true" : "false" }},
            "cellAlign": "{{ (count($sponsors) > 5) ? "left" : "center" }}",
            "contain": true,
            "initialIndex": "{{ (count($sponsors) > 5) ? "0" : "1" }}",
            "prevNextButtons": {{ (count($sponsors) > 5) ? "true" : "false" }},
            "draggable": ">2",
            {{-- "autoPlay": true, --}}
            "freeScroll": false,
            "friction": 0.8,
            "selectedAttraction": 0.2} '>
            @foreach ($sponsors as $sponsor)
            {{-- col-12 col-sm-4 col-lg-2  --}}
            <div class="img-slide bg-white d-flex align-items-center justify-content-center mx-2 py-3 px-4">
                <a href="//{{ $sponsor->url }}" target="{{ $sponsor->target }}">
                    <img src="{{ asset($sponsor->avatar)}}" class="w-100">
                </a>
            </div>
            @endforeach
        </div>
        @endif
    </div>
</div>