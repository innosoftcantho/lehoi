<div class="news_2 bg-overview pb-lg-5">
    <div class="container py-3">
        <h2 class="text-uppercase text-white font-weight-bold wow fadeInRight py-3 mb-3">
            {{ $sections[1]->category_name ?? "" }}
        </h2>
        <div class="row d-none d-lg-flex">
            <div class="col-12 bg-overview mb-3">
                @forelse ($contents_2 ?? [] as $content_2)
                    @if ($loop->odd)
                    <div>
                        <a href="{{ $sections[1]->menu() ? url($sections[1]->menu()->alias, [$content_2->alias]) : "#" }}">
                            <div class="media bg-dark1 wow fadeInLeft">
                                <img src="{{ asset($content_2->avatar)}}" class="align-self-center">
                                <div class="media-body p-3">
                                    <h6 class="font-weight-bold mb-0">
                                        {{ $content_2->title }}
                                    </h6> 
                                    <div class="media-text">
                                        <div class="my-2">
                                            <small class="font-italic">{{ $content_2->user->username }} - {{ date('d-m-Y H:i', strtotime($content_2->title)) }}</small>
                                        </div>
                                        <h4 class="font-weight-normal">
                                            {{ $content_2->summary }}
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    @else
                    <div>
                        <a href="{{ $sections[1]->menu() ? url($sections[1]->menu()->alias, [$content_2->alias]) : "#"}}">
                            <div class="media bg-dark2 wow fadeInRight">
                                <div class="media-body p-3">
                                    <h6 class="font-weight-bold mb-0">
                                        {{ $content_2->title }}
                                    </h6> 
                                    <div class="media-text">
                                        <div class="my-2">
                                            <small class="font-italic">
                                                {{ $content_2->user->username }} - {{ date('d-m-Y H:i', strtotime($content_2->title)) }}
                                            </small>
                                        </div>
                                        <h4 class="font-weight-normal">
                                            {{ $content_2->summary }}
                                        </h4>
                                    </div>
                                </div>
                                <img src="{{ asset($content_2->avatar)}}" class="align-self-center">
                            </div>
                        </a>
                    </div>
                    @endif
                @empty
                @endforelse
            </div>
        </div>
        <div class="row d-lg-none">
            <div class="col-12 bg-overview mb-3">
                @forelse ($contents_2 ?? [] as $content_2)
                    <div>
                        <a href="{{ $sections[1]->menu() ? url($sections[1]->menu()->alias, [$content_2->alias]) : "#" }}">
                            <div class="card vert border-0">
                                <div class="img-vert">
                                    <img src="{{ asset($content_2->avatar)}}" class="card-img-top rounded-0">
                                </div>
                                <div class="card-body bg-dark2">
                                    <div class="card-title text-justify font-weight-bold mb-0">
                                        {{ $content_2->title }}
                                    </div>
                                    <div class="card-text text-justify">
                                        <div class="my-1">
                                            <small class="font-italic">
                                                {{ $content_2->user->username }} - {{ date('d-m-Y H:i', strtotime($content_2->title)) }}
                                            </small>
                                        </div>
                                        <h4 class="font-weight-normal">
                                            {{ $content_2->summary }}
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    @empty
                @endforelse
            </div>
        </div>
    </div>
</div>