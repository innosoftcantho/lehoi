<div class="section-slide">
    <div class="container-fluid p-0">
        @isset($carousels)
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="false">
            <ol class="carousel-indicators">
                @foreach ($carousels as $carousel)
                <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? "active" : "" }}"></li>
                @endforeach
            </ol>
            <div class="carousel-inner d-flex align-items-center">
                @foreach ($carousels as $carousel)
                <div class="carousel-item {{ $loop->first ? "active" : "" }} ">
                    <img class="w-100" src="{{ asset($carousel->avatar)}}" alt="First slide">
                </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        @endisset
    </div>
</div>