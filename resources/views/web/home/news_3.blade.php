<div class="section-news">
    <div class="container">
        <h2 class="text-uppercase font-weight-bold wow fadeInLeft py-3 my-3">
            {{ $sections[2]->category_name ?? "" }}
        </h2>
        <div class="row pb-4">
            <div class="col-xl-8">
                <div class="row">
                    @forelse ($contents_3 ?? [] as $content_3)
                        @if ($loop->index < 2)
                        <div class="col-lg-6 mb-3 mb-lg-5">
                            <a href="{{ $sections[2]->menu() ? url($sections[2]->menu()->alias, [$content_3->alias]) : "#" }}">
                                <div class="card news-2 wow fadeInLeft border-0 h-100">
                                    <div class="img-vert-2 d-flex align-items-center justify-content-center">
                                        <img src="{{ asset($content_3->avatar)}}" class="card-img-top rounded-0 h-100">
                                    </div>
                                    <div class="card-body vert p-0 mt-1">
                                        <h6 class="card-title text-justify font-weight-bold mb-0">
                                            {{ $content_3->title }}
                                        </h6> 
                                        <div class="card-text text-secondary text-justify">
                                            <div class="my-1">
                                                <small class="font-italic">{{ $content_3->user->username }} - {{ date('d-m-Y H:i', strtotime($content_3->created_at)) }}</small>
                                            </div>
                                            <h4 class="font-weight-normal">
                                                {{ $content_3->summary }}
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @else
                        <div class="col-lg-4">
                            <a href="{{ $sections[2]->menu() ? url($sections[2]->menu()->alias, [$content_3->alias]) : "#" }}" class="d-block d-lg-none">
                                <div class="card wow fadeInLeft border-0 h-100" style="width:100%;">
                                    <div class="news d-flex mb-4">
                                        <div class="col-4 px-0">
                                            <div class="d-flex align-items-center justify-content-center h-100">
                                                <img src="{{ asset($content_3->avatar)}}" class="w-100">
                                            </div>
                                        </div>
                                        <div class="col-8 pr-0">
                                            <div class="card-body p-0">
                                                <h4 class="card-title text-justify font-weight-bold mb-0">
                                                    {{ $content_3->title }}
                                                </h4> 
                                                <div class="card-text text-secondary text-justify">
                                                    <small class="d-block d-lg-none font-italic">{{ $content_3->user->username }} - {{ date('d-m-Y H:i', strtotime($content_3->created_at)) }}</small>
                                                    <h5 class="font-weight-normal">
                                                        {{ $content_3->summary }}
                                                    </h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="{{ $sections[2]->menu() ? url($sections[2]->menu()->alias, [$content_3->alias]) : "#" }}" class="news-2 d-none d-lg-block">
                                <div class="wow fadeInLeft">
                                    <h4 class="card-title text-justify font-weight-bold mb-2">
                                        {{ $content_3->title }}
                                    </h4>
                                    <div class="d-flex">
                                        <div class="col-4 px-0">
                                            <div class="d-flex align-items-start justify-content-center h-100 ">
                                                <img src="{{ asset($content_3->avatar)}}" class="w-100">
                                            </div>
                                        </div>
                                        <div class="col-8 pr-0">
                                            <div class="card-body p-0">
                                                <div class="card-text text-secondary text-justify">
                                                    <h5 class="font-weight-normal">
                                                        {{ $content_3->summary }}
                                                    </h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <small class="font-italic text-secondary mt-1">{{ $content_3->user->username }} - {{ date('d-m-Y H:i', strtotime($content_3->created_at)) }}</small>
                                </div>
                            </a>
                        </div>
                        @endif
                    @empty
                        
                    @endforelse
                </div>
            </div>
            <div class="col-xl-4 d-none d-xl-block">
                @isset($banners)
                    @foreach ($banners as $banner)
                        @if ($loop->index >1)
                        <a href="#">
                            <div class="image-2 wow fadeInLeft mb-3">
                                <img src="{{ asset($banner->avatar)}}" class="w-100 h-100">
                            </div>
                        </a>
                        @endif
                    @endforeach
                @endisset
            </div>
        </div>
    </div>
</div>