<div class="section-news">
    <div class="container">
        <h2 class="text-uppercase font-weight-bold wow fadeInLeftBig py-3 my-3">
            {{ $sections[0]->category_name ?? "" }}
        </h2>
        <div class="row wow fadeInRightBig">
            @isset($contents_1)
            <div class="col-lg-4 mb-3">
                <a href="{{ $sections[0]->menu() ? url($sections[0]->menu()->alias, [$contents_1[0]->alias]) : "#" }}">
                    <div class="card news border-0 h-100">
                        <div class="img-vert d-flex align-items-center justify-content-center">
                            <img src="{{ asset($contents_1[0]->avatar)}}" class="card-img-top rounded-0 h-100">
                        </div>
                        <div class="card-body vert p-0 mt-1">
                            <div class="card-title text-justify font-weight-bold mb-0">
                                {{ $contents_1[0]->title }}
                            </div> 
                            <div class="card-text text-secondary text-justify">
                                <div class="my-1">
                                    <small class="font-italic">{{ $contents_1[0]->user->username }} - {{ date('d-m-Y H:i', strtotime($contents_1[0]->created_at)) }}</small>
                                </div>
                                <h4 class="font-weight-normal">
                                    {{ $contents_1[0]->summary }}
                                </h4>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endisset
            <div class="col-lg-8">
                <div class="row">
                    @forelse ($contents_1 ?? [] as $content1)
                        @if (!$loop->first)
                        <div class="col-lg-6 mb-0">
                            <a href="{{ $sections[0]->menu() ? url($sections[0]->menu()->alias, [$content1->alias]) : "#" }}">
                                <div class="card border-0" style="width:100%;">
                                    <div class="news d-flex mb-4">
                                        <div class="col-4 px-0">
                                            <div class="d-flex align-items-start justify-content-center h-100">
                                                <img src="{{ asset($content1->avatar)}}" class="w-100">
                                            </div>
                                        </div>
                                        <div class="col-8 pr-0">
                                            <div class="card-body p-0">
                                                <h4 class="card-title text-justify font-weight-bold mb-0">
                                                    {{ $content1->title }}
                                                </h4>
                                                <div class="card-text text-secondary text-justify">
                                                    <small class="font-italic">{{ $content1->user->username }} - {{ date('d-m-Y H:i', strtotime($content1->created_at)) }}</small>
                                                    <h5 class="font-weight-normal">
                                                        {{ $content1->summary }}
                                                    </h5>
                                                </div>
                                            </div>   
                                        </div> 
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endif
                    @empty
                        
                    @endforelse
                </div>
            </div>
        </div>
        <div class="row wow fadeInUp mb-4">
            @isset($banners)
                @foreach ($banners as $banner)
                    @if ($loop->index <2)
                    <div class="col-lg-6">
                        <a href="#">
                            <div class="image mb-3">
                                <img src="{{ asset($banner->avatar)}}" class="w-100 h-100">
                            </div>
                        </a>
                    </div>
                    @endif
                @endforeach
            @endisset
        </div>
    </div>
</div>