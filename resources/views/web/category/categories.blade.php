<div class="categories pb-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <h2 class="text-uppercase font-weight-bold py-4">
                    bài viết mới nhất
                </h2>
                @foreach ($contents as $content)
                <div class="col-12 mb-4 px-0">
                    <a href="{{ url($menu->alias, [$content->alias]) }}">
                        <div class="media">
                            <img src="{{ asset($content->avatar)}}" class="align-self-center">
                            <div class="media-body px-3">
                                <h6 class="font-weight-bold text-body mb-0">
                                    {{ $content->title }}
                                </h6>
                                <div class="text-secondary d-none d-sm-block">
                                    <small class="font-italic">
                                        {{ $content->user->username }} - {{ \Carbon\Carbon::parse($content->created_at)->diffForHumans() }}
                                    </small>
                                    <h4 class="mt-1">
                                        {{ $content->summary }}
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
            <div class="col-lg-3 h-100 pt-lg-4">
                <div class="bg-light border px-4 py-3">
                    <h3 class="text-uppercase font-weight-bold mb-4">danh mục bài viết</h3>
                    <ul class="navbar-nav flex-column d-block text-uppercase sidebar">
                        @foreach ($categories as $category)
                        @if (count($category->children) > 0)
                        <li class="text-uppercase {{ (!$loop->first && !$loop->last) ? "my-1" : "" }}">
                            <a href="#category_{{ $category->id }}" data-toggle="collapse" aria-expanded="false">
                                <span>{{ $category->menu_name }}</span>
                                <span class="dropdown-toggle dropdown-toggle-split">
                                </span>
                            </a>
                            <ul class="collapse list-unstyled pl-4" id="category_{{ $category->id }}">
                                @foreach ($category->children as $children)
                                <li>
                                    <a href="{{ url($children->alias) }}">{{ $children->menu_name }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        @else
                        <li class="text-uppercase {{ (!$loop->first && !$loop->last) ? "my-3" : "" }}">
                            <a href="{{ url($category->alias) }}">
                                <span>{{ $category->menu_name }}</span>
                            </a>
                        </li>
                        @endif
                        @endforeach
                    </ul>
                </div>
                <div class="d-none d-lg-block mt-3">
                    @isset($banners)
                        @foreach ($banners as $banner)
                        <a class="w-100" href="/content">
                            <img src="{{ asset($banner->avatar) }}" class="img-fluid w-100 mb-2">
                        </a>
                        @endforeach
                    @endisset
                </div>
            </div>
            {{ $contents->links('web.category.paginate') }}
        </div>
    </div>
</div>