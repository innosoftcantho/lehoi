<div class="overview-2 mb-3">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 pr-lg-1 mt-3 mb-2 mb-lg-0">
                <div class="row">
                    @if (count($featured_contents) > 0)
                    @foreach ($featured_contents as $content)
                        @if ($loop->first)
                        <div class="col-12">
                            <a href="{{ url($menu->alias, [$content->alias]) }}">
                                <div class="card vert border-0" style="width:100%;">
                                    <div class="img-vert">
                                        <img src="{{ asset($content->avatar)}}" class="w-100 h-100">
                                    </div>
                                    <div class="card-body bg-dark2">
                                        <h3 class="card-title text-justify font-weight-bold mb-0">
                                            {{ $content->title }}
                                        </h3> 
                                        <div class="card-text text-justify">
                                            <div class="my-1">
                                                <h4 class="font-italic">{{ $content->user->username }} - {{ date('d-m-Y h:i', strtotime($content->created_at))}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        @elseif ($loop->index < 3)
                        <div class="col-lg-6 p{{ $loop->index == 1 ? "r" : "l" }}-lg-1  mt-2">
                            <a href="{{ url($menu->alias, [$content->alias]) }}">
                                <div class="card horz border-0 h-100 w-100">
                                    <div class="d-flex h-100">
                                        <div class="col-5 bg-dark2 px-0">
                                            <div class="img-horz d-flex align-items-center justify-content-center h-100">
                                                <img src="{{ asset($content->avatar)}}" class="w-100">
                                            </div>
                                        </div>
                                        <div class="col-7 bg-dark2">
                                            <div class="card-body px-0 py-2">
                                                <h4 class="card-title font-weight-bold mb-0">
                                                    {{ $content->title }}
                                                </h4> 
                                                <div class="card-text">
                                                    <div class="my-1">
                                                        <small class="font-italic">{{ $content->user->username }} -  {{ date('d-m-Y h:i', strtotime($content->created_at))}}</small>
                                                    </div>
                                                </div>
                                            </div>   
                                        </div> 
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endif
                        
                    @endforeach
                    @endif
                </div>
            </div>
            <div class="col-lg-4 pl-lg-1 mt-lg-3">
                @foreach ($featured_contents as $content)
                    @if ($loop->index >2)
                    <a href="{{ url($menu->alias, [$content->alias]) }}">
                        <div class="card vert-2 border-0 pb-2" style="width:100%;">
                            <div class="img-vert-2">
                                <img src="{{ asset($content->avatar)}}" class="h-100 w-100">
                            </div>
                            <div class="card-body bg-dark2 py-3">
                                <h3 class="card-title text-justify font-weight-bold mb-0">
                                    {{ $content->title }}
                                </h3> 
                                <div class="card-text text-justify">
                                    <div class="my-1">
                                        <h4 class="font-italic">{{ $content->user->username }} - {{ date('d-m-Y H:i', strtotime($content->created_at)) }}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>