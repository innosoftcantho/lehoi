<?php

namespace App\Http\Controllers\Admin;
use App\Customer;
use App\City;
use App\District;
use App\Ward;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCustomer;
use App\Http\Requests\UpdateCustomer;
use Carbon\Carbon;

class CustomerController extends Controller
{
    public function __construct(Customer $model)
    {
        $this->model    = $model;
        $this->slug     = 'customers';
        $this->city     = new City;
        $this->district = new District;
        $this->ward     = new Ward;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.$this->slug.index",[
            'data_table'    => $this->model->read(),
            'route'         => $this->slug,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.$this->slug.create",[
            'route'         => $this->slug,
            'cities'        => $this->city->all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCustomer $request)
    {
        $customer = $this->model->create($request->all());
        $customer->cities()->attach($request->city_id);
        $customer->districts()->attach($request->district_id);
        $customer->wards()->attach($request->ward_id);
        return redirect()->route("$this->slug.index")->with('success', 'create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        return view("admin.$this->slug.edit",[
            'cities'    => $this->city->all(),
            'districts' => $customer->cities->first()->districts,
            'wards'     => $customer->districts->first()->wards,
            'data'      => $customer,
            'route'     => $this->slug,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCustomer $request, Customer $customer)
    {
        $customer->update($request->all());
        $customer->cities()->sync($request->city_id);
        $customer->districts()->sync($request->district_id);
        $customer->wards()->sync($request->ward_id);
        return redirect()->route("$this->slug.index")->with('success', 'update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $customer->delete();
        return redirect()->route("$this->slug.index")->with('success', 'delete');
    }
}
