<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Content;
use App\Language;
use App\Menu;
use App\MenuCategory;
use App\MenuContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMenu;
use App\Http\Requests\UpdateMenu;

class MenuController extends Controller
{
    public function __construct(Menu $model)
    {
        $this->model            = $model;
        $this->slug             = $model->getTable();
        $this->category         = new Category;
        $this->content          = new Content;
        $this->language         = new Language;
        $this->menu_category    = new MenuCategory;
        $this->menu_content     = new MenuContent;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.$this->slug.index",
        [
            'data_table'    => $this->model->read(),
            'route'         => $this->slug,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.$this->slug.create",
        [
            'categories'    => $this->category->get(),
            'contents'      => $this->content->get(),
            'menus'         => $this->model->get(),
            'route'         => $this->slug,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMenu $request)
    {
        $menu = $this->model->create($request->merge([
                'menu_id'   => $this->model->getMenuId(),
                'lang'      => $this->language->defaultLang(),
            ])->all());
        $request->merge([
            'menu_id'   => $menu->id
        ]);
        if ($menu->type == 'about' || $menu->type == 'content')
        {
            $this->menu_content->create($request->only(['menu_id', 'content_id']));
        }
        elseif ($menu->type == 'home') {
            $sections = [$request->category_id_1, $request->category_id_2, $request->category_id_3, $request->carousel, $request->banner];
            if( count($sections) != count(array_unique($sections)) )
            {
                return redirect()->back()->with('error', 'Danh mục bị trùng');
            }
            foreach ($sections as $index => $value)
            {
                // $this->category->find($value)->update(['sort' => $index+1]);
                $menu->categories()->attach($value, ['sort' => ($index + 1)]);
            }
        }
        else if ($menu->type == 'category')
        {
            $menu->categories()->attach([
                $request->category_id   => ['menu_categories.sort'  => 1],
                $request->banner       => ['menu_categories.sort'  => 2],
            ]);
            // $this->menu_category->create($request->only(['menu_id', 'category_id']));
        }
        return redirect()->route("$this->slug.index")->with('success', 'create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        return view("admin.$this->slug.edit",
        [
            'menu_category' => $menu->menu_category,
            'menu_content'  => $menu->menu_content,
            'sections'      => $menu->categories->sortBy('pivot.sort')->values(),
            'categories'    => $this->category->get(),
            'contents'      => $this->content->get(),
            'menus'         => $this->model->get()->except($menu->id),
            'data'          => $menu,
            'route'         => $this->slug,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMenu $request, Menu $menu)
    {
        $request->merge([
            'target'    => ($request->target ?? '')
        ]);
        $menu->update($request->all());
        $request->merge([
            'menu_id'   => $menu->id,
        ]);
        if ($menu->type == 'about' || $menu->type == 'content')
        {
            $menu->menu_contents()->delete();
            $this->menu_content->create($request->only(['menu_id', 'content_id']));
        }
        elseif ($menu->type == 'home') {
            $sections = [
                0   => $request->category_id_1 ?? '',
                1   => $request->category_id_2 ?? '',
                2   => $request->category_id_3 ?? '',
                3   => $request->carousel ?? '',
                4   => $request->banner ?? ''];
            if( count($sections) != count(array_unique($sections)) )
            {
                return redirect()->back()->with('error', 'Danh mục bị trùng');
            }
            $menu->categories()->detach();

            foreach ($sections as $index => $value)
            {
                $menu->categories()->attach($value, ['sort' => ($index + 1)]);
            }
        }
        else if ($menu->type == 'category' )
        {
            $menu->categories()->detach();
            $menu->categories()->attach([
                $request->category_id   => ['menu_categories.sort'    => 1],
                $request->banner        => ['menu_categories.sort'    => 2],
            ]);
            // $menu->categories()->sync($request->only(['category_id', 'banner']));
        }
        if ($request->previous == url()->previous())
            return redirect()->route("$this->slug.index")->with('success', 'update');
        return redirect($request->previous)->with('success', 'update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        // dd($menu->categories);
        $menu->menu_categories()->delete();
        $menu->delete();
        return redirect()->route("$this->slug.index")->with('success', 'delete');
    }
}
