<?php

namespace App\Http\Controllers\Web;

use App\Menu;
use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function __construct(Menu $model, Product $product)
    {
        $this->menu = $model;
        $this->product = $product;
    }

    public function index($menu)
    {
        return view("web.$menu->type",
        [
            'about'     => $menu->contents()->firstOrFail(),
            'menu'      => $menu,
            'menus'     => $this->menu->getMenus(),
            'all_products'  => $this->product->get(),
            'route'     => $menu->type,
        ]);
    }
}