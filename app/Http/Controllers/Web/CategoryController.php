<?php

namespace App\Http\Controllers\Web;

use App\Content;
use App\Menu;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct(Menu $model, Content $content)
    {
        $this->content  = $content;
        $this->menu     = $model;
    }

    public function index($menu)
    {
        $category = $menu->categories->sortBy('pivot.sort')->values()->first() ?? abort(404);
        $banner = (count($menu->categories) > 1) ? $menu->categories()->where('id', '<>', $category->id)->firstOrFail()->contents : null;
        $featured = $category->contents()->where('contents.is_featured', 1)->where('contents.is_show', 1)->orderBy('contents.sort', 'desc')->get();
        if ($featured->count() == 0)
            $featured = $category->contents()->where('contents.is_featured', 0)->where('contents.is_show', 1)->orderBy('contents.id', 'desc')->orderBy('contents.sort', 'desc')->take(5)->get();
        $contents = $category->contents()->where('contents.is_featured', 0)->where('contents.is_show', 1)->whereNotIn('contents.id', $featured->pluck('id'))->orderBy('contents.id', 'desc')->paginate(5);
        return view("web.$menu->type",
        [
            'contents'          => $contents,
            'latests'           => $this->content->orderBy('created_at')->take(5)->get(),
            'featured_contents' => $featured,
            'category'          => $category,
            'categories'        => $this->menu->getMenus()->where('type', 'category'),
            'banners'           => $banner,
            'menu'              => $menu,
            'menus'             => $this->menu->getMenus(),
            'route'             => $menu->type,
        ]);
    }

    public function detail($menu, $alias)
    {
        $category = $menu->categories()->firstOrFail();
        $content = $this->content->firstAlias($alias);
        $contents = $category->contents()->where('contents.is_show', 1)->where('contents.id', '<>', $content->id)->orderBy('id', 'desc')->paginate(3);
        return view("web.content",
        [
            'contents'  => $contents,
            'content'   => $content,
            'categories'=> $this->menu->getMenus()->where('type', 'category'),
            'category'  => $category,
            'menu'      => $menu,
            'menus'     => $this->menu->getMenus(),
            'route'     => $menu->type,
        ]);
    }
}