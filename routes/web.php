<?php

use App\Http\Middleware\Activated;
use App\Http\Middleware\Admin;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false, 'verify' => true]);

Route::middleware(['auth', Activated::class])->group(function () {
    Route::prefix('admin')->group(function () {
        Route::namespace('Admin')->group(function () {
            Route::middleware(Admin::class)->group(function () {
            });
            
            Route::get('/', 'HomeController@index')->name('admin');
            Route::get('profile', 'UserController@profile')->name('profile');
    
            Route::resource('categories', 'CategoryController')->except(['show']);
            Route::resource('contacts', 'ContactController')->except(['create', 'edit', 'update']);
            Route::resource('contents', 'ContentController')->except(['show']);
            Route::resource('languages', 'LanguageController')->except(['show']);
            Route::resource('menus', 'MenuController')->except(['show']);
            Route::resource('sponsors', 'SponsorController')->except('show');
            Route::resource('users', 'UserController')->except(['show']);
        });
    });
});

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::post('admin', 'Admin\ContactController@store')->name('web.contacts.store');
    Route::get('{menu}', 'HomeController@menu')->where('menu', '([A-Za-z0-9\-\/]+)');
});